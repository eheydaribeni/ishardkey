*The latest compiled and ready-to-read PDF file is located in:
**Source > Emad-report.pdf**
*

We studied most challenges of Database Partitioning in general, Sharding in specific. Furthermore, the customised approaches of two successful vendors have been reviewed. As matter of fact that choosing a set of decent Shard keys in the beginning stage of database designing is unavoidably significant for the future performance of the whole application, we introduced a way of auto-detecting the candidate Shard Keys. That was done by considering query logs and forming the *cost vectors* holding information about attributes of all relations within predicates. Finally, with a decision-tree based approach, we presented candidate single or compound Shard keys. In addition, we proposed a relatively same approach for detection of appropriate Sharding scheme, either Range-based or Hash-based one.


Keywords:  Database Sharding, Partitioning, Sharding, Shard Key

Emad Heydari Beni
Universiteit Antwerpen,
Belgium