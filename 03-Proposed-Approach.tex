\chapter{Proposed approach}
In this chapter we introduce a decision-based approach for an automatic estimation/suggestion of Sharding scheme with appropriate Shard keys based on SQL query logs.

\section{Shard Keys}
\subsection{Considerations}

\textbf{Cardinality/Granularity. }The chosen field as a Shard key should have enough different values in order to enable the Shard selector to distribute the data easily across the cluster. Entity ID's, timestamps, phone numbers and these kinds of fields might be the options for Shard keys due to their high cardinality.\\

\textbf{In all queries. }The selected Shard key should exist in all/most queries. Because of the fact that Shard selectors are obliged to forward the queries to an appropriate Shard holding the data, this field should appear in most queries, which introduces Query Isolation. \\

\textbf{Compound Shard Keys. }Sometimes, it is not easy to find a field repeating in all queries or with very low cardinality if exists. In these cases, one should select several keys as compound Shard Key.\\

According to \cite{1-12}, The most performant queries in Sharded databases are those which are forwarded to just a shard. In most of the databases, if a query does not contain the shard key, they forward the query to all shards and wait for response from any of them. It is better to use 'compound key'; because if only one of the fields among compound keys exists in our query, shard manager may query smaller range of shard instances to fetch the data.\\

The \textit{Primary Keys} are potential choice for Shard Keys. These keys have very high cardinality due to their uniqueness. Sometimes these keys are random which escalate the difficulties for query routers in Shard selection process.\\

\textbf{Foreign Keys. }These types of keys should be taken into consideration when one wants to choose an appropriate Shard Key. By monitoring the SQL queries in a database, sometimes some queries come with \textit{WHERE} clauses, which may trigger the database to create joined tables. Since target rows might be resided on different machines in a very large Sharded clusters, even a simple Join forces the Query router to send sub-queries to several shards to collect data and join this data from various nodes and returns it to the query initiator, which introduces latency due to current load of the cluster, network traffic, unnecessary data movements. It is more convenient to detect mostly used Join queries by learning process on users' queries and select the Shard Keys with respect to Foreign Keys. In other words, one should desing the Sharded cluster and select widely-used Foreign Keys as shard keys. This may guarantee that rows with same primary key values and with same Foreign key values are stored in one machine, which limits the workload of result preparation of a repeated Join query to a single shard, not several shards.\\


\subsection{Sharding Schemes}\footnote{These Sharding/Partitioning schemes are explained in chapter 1.}

\subsubsection{Range-based Sharding}
This scheme is easy to implement and deploy; but maintenance is not easy to do. Because, it is likely to have uneven data distribution across shards, so called \textit{Data Skew}, and this has to be rebalanced which is sometimes not easy.\\

Choosing an appropriate Shard key in Range-based Sharding is also not trivial. For instance, if a timestamp field is selected for the Shard Key of a table, it may introduce \textit{Write Scalability} issues in some write-intensive scenarios. Because the query routers are oblige to forward the Write operations to the partition holding the latest updates, and this database instance might get easily overloaded by dozens of DML\footnote{Data Manipulation Language} operations while other instances are responsible for older data. (e.g. in case of applications such as \textit{Twitter}, those old instances may not even be read by users in due course.).\\

Any increasing-value field as a Shard key may create such problems. These shards should be continuously monitored with appropriate Administrator notifications in order to let them split the shard properly and on time.\\

\subsubsection{Hash-based Sharding}
This approach promises that the Sharded cluster ends up with evenly distributed data across all partitions, which eliminates the data rebalancing. But, this Sharding scheme is prone to considerable performance issues in case of scenarios with numerous \textit{Range} queries. Because, Shards are determined by hashed value of Shard Keys, and the probability of having two contiguous row on the same machine is very low. Query router therefore is obliged to query several shards for a very simple Range query, which is violating Query Isolation measures. Furthermore, it is not simple to use compound keys in such schemes.\\

\section{Automatic Selection}

Initially, we have a big set of given SQL queries noted with \textit{Q} along with database schema information including relations (tables), attributes (columns) with data types, and primary keys. There should be a learning tool responsible for reading queries one by one and constructing our information vector. We have following data listed in table \ref{tab:3-1}.

\begin{table}[H]
	\caption{Available data about the database.}
	\begin{tabular}{| p{4cm} | p{9cm} |}
		\hline
		\textbf{} & \textbf{Available Information}\\
		\hline
		Query log &
		Q\subscript{1}\dots Q\subscript{n}
		\\ \hline
		Database schema &
		Database relations (tables), 
		Foreign Keys, 
		Primary Keys, 
		Column Data Types
		\\ \hline
		Input & Some constant values as input for our algorithm. 
		\\ \hline
	\end{tabular}
	\label{tab:3-1}
\end{table}

\subsection{Cost Vector}
While the learning tool is processing all queries, it constructs a \textit{Cost vector} holding some information about the queries. As matter of fact that numerous forms of SQL queries are being used in applications, we just focus on very simple queries without JOIN's and other functions like GROUP BY, COUNT, etc. For simplification, we consider small queries with simple WHERE clauses as follows.\\

\begin{verbatim}
SELECT column_name, ...
FROM table_name
WHERE column_name operator value operator ...
\end{verbatim}

Every typical relational database contains several tables that we call them R\footnote{Relation}, and each R is composed of multiple attributes\footnote{Columns}. The cost vector is based on the occurrence of attributes in the Where clause.\footnote{In the cost vector, we only care about the occurrence of attributes and their values in Where clauses.}

\begin{equation*}
\begin{cases}
R_{i} \colon \text{Relation i}\\

n(R_{i}) \colon \text{Number of rows stored in Relation i.}\\

A_{i,j} \colon \text{Attribute j of Relation i.}\\

n(A_{i,j}) \colon \text{Number of occurence of A\subscript{i,j} in queries.}\\

dv(A_{i,j}) \colon \text{Number of distinct values of A\subscript{i,j} as operand in Where clauses.}\\

op(A_{i,j}, operator) \colon \text{Number of occurence of A\subscript{i,j} as operand with a particular operator.}\\

n_{update}(A_{i,j}) \colon \text{Number of occurence of A\subscript{i,j} as operand in Update queries.}\\

pr(A_{i,j}, A_{i,m}) \colon \text{Number of occurence of A\subscript{i,j} along with A\subscript{i,m} in a predicate.}\\
\end{cases}
\end{equation*}

cost(\textit{A\subscript{i,j}}) = (n(\textit{A\subscript{i,j}}), dv(\textit{A\subscript{i,j}}), op(\textit{A\subscript{i,j}, =}), op(\textit{A\subscript{i,j}, range}), n\subscript{update}(\textit{A\subscript{i,j}}), pr(\textit{A\subscript{i,j}, A\subscript{i,1}}), ... , pr(\textit{A\subscript{i,j}, A\subscript{i,m}}))\\

We have several type of operators in SQL queries which are common among most vendors, we only consider operators like (in)equality, range (i.e. BETWEEN, $>$, $<$, $>=$, $<=$) in this state of literature.\\

\textbf{Excluding large attributes.} Since we know the attributes data types, we can exclude some of them from the calculation of cost vectors. Some attributes with types such as Text, (Var)char(n$>$100), Blob, etc. that are too large should not be considered in the algorithm. Because those attributes hold data which are in appropriate for Shard key selection.\\

\textbf{Excluding reference tables.} Reference tables are those type of table holding small number of rows and are necessary for lots of queries. These tables are being updated not so often. If the Sharding vendor supports having reference tables, these tables should be replicated completely to all Shards. We will propose a simple automatic approach to detect these tables and exclude them from cost vector calculations.\\

\subsection{Sharding Schema}
We should introduce two important challenges in distributed databases with Partitioning schemes: \textit{Query Isolation}, and \textit{Write Scalability}.\\

\textbf{Query Isolation. }Shard coordinators are responsible for forwarding the queries to the appropriate Shard(s) based on Shard keys in order to fetch the data and send it back to users who are the query initiators. If the Shard key is not present in the query, the coordinator is obliged to send the query to all or some of the Shards for finding the data. But, queries containing Shard keys are more efficient due to the fact that they are sent to less number of Shards.\\

\textbf{Write Scalability. }As discussed before, Write scalability is about the ability of the coordinator to forward Write queries to various machines. For example, if the Sharding scheme is range-based and timestamps as our Shard key, all Write queries are pointing the last shard holding the recent data, in which we may end up with various difficulties with that Shard such as capacity shortage, IO latency, processing overload. It is therefore better to distribute our Write queries to the whole cluster.\\

To recap, we can somehow infer that one may have better Query Isolation while using \textit{range-based sharding}, and better Write Scalability while using \textit{hash-based Sharding}. These two concepts are the main fundamentals of our following calculations.\\

In this part we want to estimate that which Sharding scheme is more appropriate for our database based on existing query logs: Range-based or Hash-based. Firstly, we want to figure out from query logs that our application is whether \textit{Write-oriented}, \textit{Read-oriented}, or \textit{Balanced}.\\

In order to achieve this goal, we divide all queries in two types: \textit{Read} and \textit{Write}. Read queries are assumed to be SELECT queries, and consequently Write queries comprises UPDATE, INSERT and DELETE. So when our tool is running and reading the queries, it should count the number of occurrence of write's and read's. Eventually, we will have following information.\\

\begin{equation*}
	\begin{cases}
		n(write) \colon \text{Number of Write queries.}\\
		n(read) \colon \text{Number of Read queries.}\\
	\end{cases}
\end{equation*}

The value of following fraction will be useful in rest of the section for Sharding scheme suggestion approach.


\begin{equation}\label{eq:3-1}
	r_{write} = \dfrac{n(write)}{n(read)}
\end{equation}

This is the initial collected information in our algorithm for auto-suggestion of Sharding scheme. If the query logs are produced over a long period of time, we may realize that whether the given application is write-oriented or read-oriented. If we have a very high rate of writes, the Sharding architecture should definitely respect Write scalability measures in order to avoid hammering any particular machine with probable massive amount of request, which may prevent unbalanced shards, IO overhead, and slow response time. In other words, if the rate of writes is considerably larger than read rate, \textit{Hash-based} sharding seems to be a decent choice for this case. Because, the Shard coordinator determine the Shard machine based on hashed Shard key(s) of each write query, which is promised to be a unique and random value.\\

On the other hand, if the read rate is much higher than the other rate, the application is read-oriented. The Query Isolation measures therefore are clearly significant.  In contrast to Write-oriented, it is not an easy decision to choose any Sharding scheme at this point in this case. In addition, there are applications with relatively close rates of writes and read that we call them Balanced query patterns. Other metrics are going to be introduced to leverage the idea to get closer to a decent estimation. Knowing the fact that we have \textit{p} relations, and \textit{m\subscript{\textit{i}}} attributes for each \textit{R\subscript{\textit{i}}}, we can calculate following values.


\begin{equation*}
\begin{cases}
\sum_{i=1}^{p}\sum_{j=1}^{m_{i}}op(A_{i,j}, =) \colon \text{Sum of all occurences of all attributes appearing in}\\ \text{(in)equality predicates for the whole database.}\\

\sum_{i=1}^{p}\sum_{j=1}^{m_{i}}op(A_{i,j}, range) \colon \text{Sum of all occurences of all attributes appearing in}\\ \text{range predicates for the whole database.}\\
\end{cases}
\end{equation*}

Then we can form another rate or fraction by division of these two values that will be useful in a short while.

\begin{equation}\label{eq:3-2}
r_{eq} = \dfrac{\sum_{i=1}^{p}\sum_{j=1}^{m_{i}}op(A_{i,j}, =)}{\sum_{i=1}^{p}\sum_{j=1}^{m_{i}}op(A_{i,j}, range)}
\end{equation}

With respect to \textit{r\subscript{\textit{write}}} and \textit{r\subscript{\textit{eq}}} calculated in \ref{eq:3-1} and \ref{eq:3-2}, we are able draw following conclusion.\\

% Flowchart styles
\tikzstyle{startstop} = [rectangle, rounded corners, minimum width=3cm, minimum height=1cm,text centered, draw=black, fill=red!30]
\tikzstyle{io} = [trapezium, trapezium left angle=70, trapezium right angle=110, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=blue!30]
\tikzstyle{process} = [rectangle, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=orange!30]
\tikzstyle{decision} = [diamond, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=green!30]
\tikzstyle{arrow} = [thick,->,>=stealth]

\begin{tikzpicture}[node distance=2cm]

\node (in1) [io] {Input
	(\textit{r\subscript{\textit{write}}}, \textit{r\subscript{\textit{eq}}})};

\node (ifGGbeta) [decision, below of=in1, yshift=-0.5cm] {\textit{r\subscript{\textit{write}}}$\gg$$\beta$};

\node (ifBetweenbeta) [decision, below of=ifGGbeta, yshift=-1.5cm] {-$\beta$$\le$\textit{r\subscript{\textit{write}}}$\le$$\beta$};


\node (ifGGalpha) [decision, right of=ifBetweenbeta, xshift=3cm] {\textit{r\subscript{\textit{eq}}}$\gg$$\alpha$};

\node (hash) [process, right of=ifGGbeta, xshift=3cm] {Hash-baed Sharding};

\node (range) [process, below of=ifGGalpha, yshift=-1cm] {Range-based Sharding};

\draw [arrow] (in1) -- (ifGGbeta);
\draw [arrow] (ifGGbeta) -- node[anchor=north] {yes} (hash);
\draw [arrow] (ifGGbeta) -- node[anchor=west] {no} (ifBetweenbeta);
\draw [arrow] (ifBetweenbeta) -- node[anchor=north] {yes} (ifGGalpha);
\draw [arrow] (ifBetweenbeta) |- node[anchor=east] {no}(range);
\draw [arrow] (ifGGalpha) -- node[anchor=west] {no} (range);
\draw [arrow] (ifGGalpha) -- node[anchor=east] {yes} (hash);

\end{tikzpicture}
\\
According to the flowchart, $\alpha$ and $\beta$ are some constants given by the database architect to be used as thresholds.

\subsection{Shard Key}
As discussed in previous section, we have calculated the cost vectors, and the suggested Sharding scheme has been decided by far. Now it is time to nominate one or several attributes per relation as our Shard key(s). Firstly, challenges and features of future Shard keys should be considered.\\

\textbf{Reference tables. }As explained earlier in this chapters, some database vendors support unpartitioned tables along with Sharded tables. In other words, these tables are replicated to all nodes, and are accessible to all database instances locally. These relations should contains small quantity of rows with low rate of Write queries. Because, every CUD\footnote{Create Update Delete} operation has to be replicated and synchronised to all machines, which may introduce either inconsistency or latency. These relations have to be waived from our cost vector calculations as well as Shard keys determination decision tree. One may select the reference tables manually before the execution of the learning tool. So if the \textit{r\subscript{read}(R\subscript{i})} is much higher than \textit{r\subscript{wite}(R\subscript{i})}, this table can be a candidate to be treated as reference table. The $\epsilon$ is a constant value defined by the database architect.

\begin{equation*}
\begin{cases}
n(R_{i}) < \epsilon \colon \text{small number of rows}\\

n_{read}(R_{i}) \gg n_{write}(R_{i})
\end{cases}
\end{equation*}

\textbf{Particular value of an attribute is dramatically getting requested.} Sometimes some values are getting queried in Where predicate of SQL operations more than others. For instance, if we have a social network application, some celebrities are target for this type of situations. If we Shard our data based on user ID's, the Shard responsible for those celebrities might get hammered by dozens of queries. In order to cope with Write queries, it is suggested to use \textit{Compound Shard Keys} to split the data in smaller partitions, not only with the user ID. The other workaround in case of massive number of Read queries is to use advanced caching mechanism in database or application level in order to avoid unnecessary IO operations.\\

\textbf{Predicates consisting of several sub-predicates. }Predicates are expressions that evaluates to a \textit{boolean} value, which are commonly being used in Where clauses. We have formed our Cost vectors based on occurrence of attributes in predicates of queries. If a particular attribute frequently appears with another attribute(s) in the predicates of queries, \textit{Compound Shard keys} should be taken into consideration. Because, compound keys let the query coordinator ask less number of Shards for data retrieval due to absence of some of the keys, which has positive impact on Query Isolation measures.\\

\textbf{The value of the existing Shard keys should not get updated. } Once a row is inserted to a Shard using its Shard key, the value of the attribute playing the Shard key role should not be touched. This update might be very expensive if it occurs frequently. Because, the row has to be fetched and removed from the shard, and the new shard must be coordinated again using the new value of the Shard key. Meaning that we may perform several IO operations.\\

\textbf{High cardinality of Shard key values. }A proposed Shard keys should have enough distinct values, so called cardinality. The \textit{Hash-based Sharding} scheme requires the value cardinality as high as possible to enable the query coordinator for even distribution of the rows among machines.\\

\textbf{Frequent occurrence of Shard keys in Where predicates with range operators. }If the candidate Shard keys frequently appears in Where predicate of operations with range operators (i.e. BETWEEN, $>$, $<$, $>=$, $<=$), they might get nominated for the final selected Shard key in \textit{Range-based} Sharding scheme. In other words, given the fact that we are going to use range-based Sharding, this feature should be treated as a positive point for our estimation algorithm and decision tree.\\

\subsubsection{Decision Algorithm}
In this section we want to select some candidate Shard keys for each table based on the calculated cost vectors. The \textit{cost(A\subscript{i,j})} stands for the vector of values for the attributes \textit{A\subscript{j}} of the relation \textit{R\subscript{i}}. We need to have some cost values formed by these vectors of values with respect to the necessary requirements of our Sharding Schemes.\\

\textbf{Hash-based Sharding. }If the Sharding Scheme is selected to be Hash-based method, we need to meet some requirements for calculation of the cost value assigned to every attributes of each relation. We call it \textit{X\subscript{i,j}}, meaning the cost value of attribute \textit{A\subscript{j}} of the relation \textit{R\subscript{i}}. The formula \ref{eq:hashCostV} describes this value.


\begin{equation}\label{eq:hashX}
X_{i,j} 
= n(A_{i,j}) 
+ \alpha dv(A_{i,j}) 
+ \underbrace{\beta op(A_{i,j}, =) 
	-  \gamma op(A_{i,j}, range)}_\text{R\subscript{eq} is important}
-  \overbrace{\mu n_{update}(A_{i,j})}^\text{negative point}
\end{equation}

As illustrated in \ref{eq:hashX}, $\alpha$, $\gamma$ and $\mu$ are constant values representing the importance of the particular metric in cost value calculation from database architect perspective. The \textit{n(A\subscript{i,j}) 
+ $\alpha$ dv(A\subscript{i,j})} shows the quantity of the repetition of the attribute within predicates as well as the level of distinct values. Since we are considering the Hash-based scheme, range queries will be treated as negative point in our estimations; in contrast, equality predicates along with distinct values are positive points.\\

The highest value of \textit{X} is important factor in our decision tree for selecting the candidate Shard key(s). But what if that all \textit{X\subscript{i,j}} are high and close to each other? How can we realize the statistical distance between these values? We propose to use \textit{Variance} of these values. Formally, the Variance shows that how far a set of numbers is spread out. In other words, if the \textit{Var(X)} is quite large, it means that the \textit{X\subscript{i,j}} values are not close to each other and the maximum cost value among attributes is going to promise that its attribute could be the candidate of the Shard key. But, if the \textit{Var(X)} holds a small value, it means that the \textit{X\subscript{i,j}} values are close to each other, which leads us to think about \textit{Compound Shard keys}. In this case, we just select two keys for simplifications; but in production you choose more.\\   


\begin{equation*}
\begin{cases}
(X_{i,1} \dots X_{i,j} \dots X_{i,m})\\
Var(X_{i}): \text{Variance of cost values for the relation i.}
\end{cases}
\end{equation*}
\\

 The following notation , \textit{max(X\subscript{i,j})}, returns the attributes owning the maximum of all cost values of its relation. The explanation is as follows.\\
 
 
\begin{equation*}\label{eq:hashCostV}
\begin{cases}
A_{i,k} \leftarrow max(X_{i,j}) \\
\text{The cost value of attribute k from relateion i}\\
\text{has the highest cost value among others.}
\end{cases}
\end{equation*}
\\
The decision tree for the selection of Shard keys with Hash-based scheme is as follows. \textit{max(X\subscript{i,j})} means that the attribute which has the highest\textit{ X\subscript{i,j}}. If the \textit{Var(X)} has a low value, we choose the mostly repeated attribute within predicates along with our first Shard Key.\\ \\

% Set the overall layout of the tree
\tikzstyle{level 1}=[level distance=3.5cm, sibling distance=3.5cm]
\tikzstyle{level 2}=[level distance=3.5cm, sibling distance=2cm]

% Define styles for bags and leafs
\tikzstyle{bag} = [text width=4em, text centered]
\tikzstyle{end} = [circle, minimum width=3pt,fill, inner sep=0pt]

\begin{tikzpicture}[grow=right, sloped]
\node[bag] {Var(X)}
	child {
		node[end, label=right:
		{\text{Shard Key (\textit{max(X\subscript{i,j}),\newline 
				max(pr(\textit{A\subscript{i,j}}, \textit{A\subscript{i,1}}) $\dots$ pr(\textit{A\subscript{i,j}}, \textit{A\subscript{i,m}}))}}}] {}
		 edge from parent
		node[below] {low value}
	}
	child {
		node[end, label=right:
		{Shard Key (\textit{max(X\subscript{i,j})})}] {}
		 edge from parent
		node[above] {high value}
	};
\end{tikzpicture}\\


\textbf{Range-based Sharding. } If this approach is selected for our Sharding scheme, we need to additionally take range queries into account. In order to achieve this goal, \textit{op(A\subscript{i,j}, range)} values within cost vectors should be counted as positive point. Then, we can form our cost values for all attributes of each relation as follows:\\ 


\begin{equation}\label{eq:rangeX}
X_{i,j} 
= n(A_{i,j}) 
+ \alpha dv(A_{i,j}) 
+ \underbrace{\beta op(A_{i,j}, =) 
	+  \gamma op(A_{i,j}, range)}_\text{range-based predicates are also important}
-  \overbrace{\mu n_{update}(A_{i,j})}^\text{negative point}
\end{equation}

As illustrated in \ref{eq:rangeX}, $\alpha$, $\gamma$ and $\mu$ are constant values representing the importance of the particular metric in cost value calculation from database architect perspective, which may be different from selected values for cost value calculation of Hash-based approach. Again, the Shard key value should not get updated once it is stored on of the Shards since this operation is too expensive. Therefore, as shown, it should be counted as negative point in our cost value.\\

\textbf{Monotonically increasing attributes. }As matter of fact that we initially know the data type of all attributes of each relation, we know that which column value is going to get increased over the time. This may cause some trouble if those attributes is selected as our Shard key. For example, if that column chosen as our Shard key is of a timestamp data type, we will be inserting new rows on a Shard responsible for recent data. This may cause latency due to IO limitations. In order to cope with this issue, our proposed workaround is again to use \textit{Compound Shard Keys} to be sure that all new rows will not reside on a particular node all the time. We should use these metrics in our decision tree.\\


% Set the overall layout of the tree
\tikzstyle{level 1}=[level distance=3.5cm, sibling distance=3.5cm]
\tikzstyle{level 2}=[level distance=3.5cm, sibling distance=2cm]

% Define styles for bags and leafs
\tikzstyle{bag} = [text width=4em, text centered]
\tikzstyle{end} = [circle, minimum width=3pt,fill, inner sep=0pt]

\begin{tikzpicture}[grow=right, sloped]
\node[bag] {Var(X)}
child {
	node[end, label=right:
	{\text{Shard Key (\textit{max(X\subscript{i,j}),\newline 
				max(pr(\textit{A\subscript{i,j}}, \textit{A\subscript{i,1}}) $\dots$ pr(\textit{A\subscript{i,j}}, \textit{A\subscript{i,m}}))}}}] {}
	edge from parent
	node[below] {low value}
}
child {
	node[bag] {type(max(\textit{X\subscript{i,j}}))}
	child {
		node[end, label=right:
		{Shard Key (\textit{max(X\subscript{i,j})})}] {}
		edge from parent
		node[below] {not increasing}
	}
	child {
		node[end] {}
		node[end, label=right:
		The same as low \textit{Var(X)}] {}
		edge from parent
		node[above] {increasing}
	}
	edge from parent
	node[above] {high value}
};
\end{tikzpicture}\\

Again, we are using \textit{Var(X)} in order to see how the set of cost values are spread out. As illustrated in the decision tree, we limit our Shard key selection to two keys for simplification.


% lot's of boundry conditions <, >, >=, and <= and BETWEEN   => good for range based partitioning

